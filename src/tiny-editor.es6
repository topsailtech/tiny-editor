import './tinyeditor'

class TinyEditor extends HTMLTextAreaElement {
  connectedCallback(){
    if(!this.id){
      console.log("Textarea must have an id to initialize tiny-editor");
      return;
    }

    if(this.editor_initialized){
      return;
    }
    this.editor_initialized = true;

    let editor = new TINY.editor.edit(this.id,{
      textarea: this,
      cssclass:'te',
      controlclass:'tecontrol',
      rowclass:'teheader',
      dividerclass:'tedivider',
      controls:['bold','italic','underline','strikethrough','|','subscript','superscript','|',
            'orderedlist','unorderedlist','|','outdent','indent','|','leftalign',
            'centeralign','rightalign','blockjustify','|','unformat','|','undo','redo','n',
            'font','size','style','|','image','hr','link','unlink','|','cut','copy','paste','print'],
      footer:true,
      fonts:['Verdana','Arial','Georgia','Trebuchet MS'],
      xhtml:true,
      // cssfile:'style.css',
      bodyid:'editor',
      footerclass:'tefooter',
      toggle:{text:'show source',activetext:'show wysiwyg',cssclass:'toggle'},
      resize:{cssclass:'resize'}
    });

    this.closest('form').addEventListener('submit', function(){
      // abuse tinyeditor code to place content into textarea
      editor.post();
    })
  }
}

customElements.define('tiny-editor', TinyEditor, { extends: "textarea" });