# Installation

```
yarn add https://bitbucket.org/topsailtech/tiny-editor.git
```

# Usage

In your pack, add
```
import("tiny-editor")
```
and reference this pack as a javascript.

<textarea is="tiny-editor"></textarea>